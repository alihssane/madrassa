package be.school.services;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by panther on 11/02/16.
 */
public class StudentService {

    public void sayHello(){
        System.out.println("hello");
    }

    public double calculateAverage(Map<String, Double> score){

        double moyenne = 0.0d;
        double total = 0;

        for(double note : score.values()){
            total += note;
        }
        moyenne = total / score.size();

        return moyenne;
    }
}
