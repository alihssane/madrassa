package be.school;

import be.school.services.StudentService;
import java.util.HashMap;
import java.util.Map;

public class Test {
    public static void main(String[] args) {

        StudentService service = new StudentService();

        Map<String, Double> score = new HashMap<String, Double>();
        score.put("Araba", 13d);
        score.put("Français", 16d);
        score.put("Maths", 11d);

        double moyenne = service.calculateAverage(score);

        System.out.println("sans: " + moyenne);
        System.out.println("round: " + Math.round(moyenne));
        System.out.println("ceil: " + Math.ceil(moyenne));
        System.out.println("floor: " + Math.floor(moyenne));
        System.out.println("abs: " + Math.abs(moyenne));
    }
}