package be.school.entities;

/**
 * Created by panther on 11/02/16.
 */
public class Student {
    private String firsttname;
    private String lastname;
    private int age;

    public Student(String firsttname, String lastname) {
        this.firsttname = firsttname;
        this.lastname = firsttname;
    }

    public String getFirsttname() {
        return firsttname;
    }

    public void setFirsttname(String firsttname) {
        this.firsttname = firsttname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }
}

